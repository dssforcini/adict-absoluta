<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'adict_absoluta' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '`P;|h#[x0g0)?$D/1?ws$Vx Brc9;7PPuLF(W1A1fWK=(|)_B7#M-9B;K!pzGv%?' );
define( 'SECURE_AUTH_KEY',  'Q)7lznJ=+cZ<f|}**=#,,dWYLITuH!Jz}<<fh!g+lgQTgp]gy?CZ2Kv%X5J[l4;*' );
define( 'LOGGED_IN_KEY',    'zboA;Z)DUEuZGqmx,~h:t?3{O/3e8NuZ<<ch6x~!G)Va4w{C->Y/x_6)v%#L<4*o' );
define( 'NONCE_KEY',        '!;b5D[!JoR/`Xoor,7BQ?p doZRhEo6+9KD0Q9mTG#crj<{E?-DY53VvNX}@thjb' );
define( 'AUTH_SALT',        'pq6vV;vy6t5N;*$_KwC0ri_Gy_n^%a~_Wr?)#<dk;8fB3s*]zF!S&:-7JK[WvQL<' );
define( 'SECURE_AUTH_SALT', 'Mga=9$SR{V>Espt-ptH* Jw}<giNk+dPLXr10fAJ-YJX1sa 37?y(.gx_a{;5WE7' );
define( 'LOGGED_IN_SALT',   'ir~?&r}K6wrB$!EGeFsvXlocDTDVKcun<e/=ow#dnp_+M4OX3C9@PiO[LTY3Ibu;' );
define( 'NONCE_SALT',       'pbd[S22<_tBoV-kdt3:,L8u=X3Hs>Y$dc5ULk(Y=Np2N9BglRGgvMqxd9q5Nj&Zs' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_adict_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
