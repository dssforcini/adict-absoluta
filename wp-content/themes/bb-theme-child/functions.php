<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );
define( 'QTD_MOVEIS_LINHA_LISTA', 3 );
define( 'CLASSES_BLOCO_IMOVEL_LISTA', 'col-sm-6 col-md-4 col-lg-4' );

// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

//  Add Owl Carousel
add_action( 'wp_enqueue_scripts', function () {
	wp_enqueue_style( 'slick_css', FL_CHILD_THEME_URL . '/css/slick.css' );
	// wp_enqueue_style( 'slick_theme_css', FL_CHILD_THEME_URL . '/css/slick-theme.css' );
	wp_enqueue_style( 'fancybox_css', FL_CHILD_THEME_URL . '/css/jquery.fancybox.min.css' );

	wp_enqueue_script( 'slick_js', FL_CHILD_THEME_URL . '/js/slick.min.js', array(), '', true );
	wp_enqueue_script( 'fancybox_js', FL_CHILD_THEME_URL . '/js/jquery.fancybox.min.js', array(), '', true );
} );

add_filter( 'widget_text', 'do_shortcode' );

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Configurações do Site',
		'menu_title'	=> 'Configurações do Site',
		'menu_slug' 	=> 'configuracoes-site',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Página Início',
		'menu_title'	=> 'Página Início',
		'parent_slug'	=> 'configuracoes-site',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Página Grupo Absoluta',
		'menu_title'	=> 'Página Grupo Absoluta',
		'parent_slug'	=> 'configuracoes-site',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Página Catálogo',
		'menu_title'	=> 'Página Catálogo',
		'parent_slug'	=> 'configuracoes-site',
	));
	
}

add_shortcode('rodape-formulario', function(){
	ob_start();
	?>
	<div class="container-fluid cf-rodape-formulario">
		<div class="row">
			<div class="col-lg-12 nopadding">
				<h2 class="titulo-fale-conosco">fale conosco.</h2>
			</div>
		</div>
		<?php echo do_shortcode('[contact-form-7 id="56" title="Contato Rodapé"]'); ?>
		<div class="row row-lojas">
			<?php $lojas = new WP_Query(array('post_type' => 'cpt_lojas')); ?>
			<?php if ($lojas->have_posts()) { ?>
				<?php while ($lojas->have_posts()) { $lojas->the_post(); ?>
					<div class="col-xs-12 col-md-12 col-lg-4 nopadding bloco-loja-rodape">
						<div class="titulo">
							<h2 class="texto"><?php the_title(); ?></h2>
						</div>
						<div class="telefones-emails">
							<?php the_field('loja_telefones_emails'); ?>
						</div>
						<div class="endereco">
							<?php the_field('loja_endereco'); ?>
						</div>
					</div>
				<?php } wp_reset_postdata(); ?>
			<?php } ?>
		</div>
	</div>
	<?php
	return ob_get_clean();
});

/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
add_shortcode('rodape-redes-sociais', function(){
	ob_start();
	?>
	<div class="container-fluid cf-rodape-rede-social">
		<div class="row">
			<div class="col-lg-12 logo-rodape" style="background-image: url('<?php echo home_url('wp-content/uploads/2019/07/logo-fundo-rodape.png'); ?>');">
				<div class="imagem">
					<img src="<?php echo home_url('/wp-content/uploads/2019/07/logo-rodape.png'); ?>" alt="">
				</div>
				<div class="descricao-rodape">
					<?php the_field('config_descricao_rodape', 'options'); ?>
				</div>
			</div>
			<div class="col-lg-12 nopadding text-center redes-sociais">
				<div class="d-inline-block rede-social rede-social-facebook">
					<a href="#">
						<img src="<?php echo FL_CHILD_THEME_URL.'/svg/facebook-rodape.svg'; ?>" alt="Facebook">
					</a>
				</div>
				<div class="d-inline-block rede-social rede-social-instagram">
					<a href="#">
						<img src="<?php echo FL_CHILD_THEME_URL.'/svg/instagram-rodape.svg'; ?>" alt="Facebook">
					</a>
				</div>
			</div>
		</div>
	</div>
	<?php
	return ob_get_clean();
});

/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
add_shortcode('banner', function(){
	ob_start();

	$slides = get_field('config_slides', 'options');
	// echo '<pre>';
	// echo print_r($slides);
	// echo '</pre>';
	echo '<div class="fundo-slider nopadding">';
		echo '<div class="slider">';
			foreach ($slides as $slide) {
				echo '<div class="slide">';
					echo '<a href="'.$slide['config_slide_link'].'" target="'.(($slide['config_slide_nova_guia']) ? '_blank' : '_self').'">';
						echo '<img src="'.$slide['config_slide_imagem'].'" class="imagem-desktop" alt="'.$slide['config_slide_titulo'].'" title="'.$slide['config_slide_titulo'].'">';
						echo '<img src="'.$slide['config_slide_imagem_mobile'].'" class="imagem-mobile" alt="'.$slide['config_slide_titulo'].'" title="'.$slide['config_slide_titulo'].'">';
					echo'</a>';
				echo'</div>';
			}
		echo '</div>';
	echo'</div>';
	?>
	<script>
		jQuery(document).ready(function($) {
			$('.slider').slick({
				slidesToShow: 1,
				dots: false,
				arrows: false,
				infinite: true,
			});
		});
	</script>
	<?php
	return ob_get_clean();
});

/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
add_shortcode('grupo-absoluta', function(){
	ob_start();
	?>
	<div class="container-fluid cf-home-grupo">
		<div class="row">
			<div class="col-lg-8 coluna-conteudo">
				<h1 class="titulo"><?php the_field('config_grupo_titulo', 'options'); ?></h1>
				<h3 class="subtitulo"><?php the_field('config_grupo_subtítulo', 'options'); ?></h3>
				<div class="descricao">
					<?php
						$descricao_grupo = get_field('config_grupo_descricao', 'options');
						$descricao_grupo = explode(' ', strip_tags($descricao_grupo));
						$total_palavras = count($descricao_grupo);
						foreach ($descricao_grupo as $index => $palavra) {
							if ($index >= 0 && ($index <= ceil(($total_palavras / 2)))) {
								echo (($index == 0) ? '<div class="d-inline-block w-50 text-justify bloco-esquerda">' : '');
								echo $palavra.' ';
								echo (($index == ceil(($total_palavras / 2))) ? '</div>' : '');
							} elseif (($index > ceil(($total_palavras / 2))) && ($index <= $total_palavras)) {
								echo (($index == (ceil(($total_palavras / 2)) + 1)) ? '<div class="d-inline-block w-50 text-justify bloco-direita">' : '');
								echo $palavra.' ';
								echo (($index == ($total_palavras - 1)) ? '<a href="'.home_url('grupo-absoluta').'">Leia mais...</a></div>' : '');
							}
						}
					?>
				</div>
			</div>
			<div class="col-lg-4 coluna-imagem">
				<img src="<?php the_field('config_grupo_imagem', 'options'); ?>" class="img-fluid" alt="Grupo Absoluta">
			</div>
		</div>
	</div>
	<?php
	return ob_get_clean();
});

/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
add_shortcode('lancamentos', function(){
	ob_start();

	$lancamentos = new WP_Query(
		array(
			'post_type' => 'cpt_produtos',
			'posts_per_page' => 4,
			// 'order' => 'DESC',
			'tax_query' => array(
				array(
					'taxonomy' => 'tax_produto_cat',
					'field' => 'slug',
					'terms' => 'lancamento'
				)
			)
		)
	);
	if ($lancamentos->have_posts()) {
		$contador = 0;
		echo '<div class="container-fluid cf-lancamento">';
			echo '<div class="row">';
				echo '<div class="col-lg-12 titulo"><h2 class="texto">LANÇAMENTOS</h2></div>';
				while ($lancamentos->have_posts()) { $lancamentos->the_post();
					if ($contador == 0) {
						echo '<div class="col-lg-6 coluna-divisor-esquerda nopadding">';
							echo '<div class="bloco-imovel-lancamento">';
								echo '<div class="imagem">';
									echo '<a href="'.get_the_permalink().'">';
										echo '<span class="nome-movel">'.get_the_title().'</span>';
										echo '<img src="'.get_the_post_thumbnail_url().'" class="foto-'.($contador+1).'" alt="'.get_the_title().'">';
									echo '</a>';
								echo'</div>';
							echo'</div>';
						echo'</div>';
					} elseif ($contador >= 1) {
						echo (($contador == 1) ? '<div class="col-lg-6 coluna-divisor-direita nopadding">' : '');
							echo ((($contador == 1) || ($contador == 3)) ? '<div class="row nomargin">' : '');
								echo ((($contador == 1) || ($contador == 2)) ? '<div class="col-lg-6 nopadding">' : '');
								echo (($contador == 3) ? '<div class="col-lg-12 nopadding">' : '');
							
									echo '<div class="bloco-imovel-lancamento">';
										echo '<div class="imagem">';
											echo '<a href="'.get_the_permalink().'">';
												echo '<span class="nome-movel">'.get_the_title().'</span>';
												echo '<img src="'.get_the_post_thumbnail_url().'" class="img-fluid foto-'.($contador+1).'" alt="'.get_the_title().'">';
											echo '</a>';
										echo'</div>';
									echo'</div>';

								echo (($contador == 3) ? '</div>' : '');
								echo ((($contador == 1) || ($contador == 2)) ? '</div>' : '');
							echo ((($contador == 2) || ($contador == 3)) ? '</div>' : '');
						echo (($contador == 3) ? '</div>' : '');
					}
					$contador++;
				} wp_reset_postdata();
			echo'</div>';
		echo'</div>';
	}

	return ob_get_clean();
});

/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
add_shortcode('destaques', function(){
	ob_start();

	$destaques = new WP_Query(
		array(
			'post_type' => 'cpt_produtos',
			'tax_query' => array(
				array(
					'taxonomy' => 'tax_produto_cat',
					'field' => 'slug',
					'terms' => 'destaque'
				)
			)
		)
	);
	if ($destaques->have_posts()) {
		echo '<div class="container-fluid cf-slider-destaque">';
			echo '<div class="row r-slider-destaque">';
				echo '<div class="col-lg-12 nopadding col-slider-destaque">';
					while ($destaques->have_posts()) { $destaques->the_post();
						echo '<div class="item-destaque" style="background-image: url('.get_the_post_thumbnail_url().');">';
							echo '<a href="'.get_the_permalink().'">';
								echo '<img src="'.FL_CHILD_THEME_URL.'/svg/coracao-destaque.svg'.'" alt="'.get_the_title().'">';
								echo '<span class="span-mais-informacao">MAIS INFORMAÇÕES</span>';
								echo '<span class="span-clique-aqui">clique aqui</span>';
							echo'</a>';
						echo'</div>';
					} wp_reset_postdata();
				echo'</div>';
			echo'</div>';
		echo '</div>';
		?>
		<script>
			jQuery(document).ready(function($) {
				$('.col-slider-destaque').slick({
					infinite: true,
					slidesToShow: 5,
					slidesToScroll: 1
				});
			});
		</script>
		<?php
	}

	return ob_get_clean();
});

/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
add_shortcode('descricao-pagina', function(){
	ob_start();
	?>
	<div class="container-fluid cf-grupo-absoluta-pagina">
		<div class="row">
			<div class="col-lg-12">
				<h3><?php the_field('config_grupo_absoluta_titulo', 'options'); ?></h3>
			</div>
			<div class="col-lg-6">
				<?php the_field('config_grupo_absoluta_descricao', 'options'); ?>
			</div>
			<div class="col-lg-6">
				<img src="<?php the_field('config_grupo_absoluta_imagem', 'options'); ?>" class="img-fluid" alt="GRUPO ABSOLUTA">
			</div>
		</div>
	</div>
	<?php
	return ob_get_clean();
});

/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
add_shortcode('lojas-paginas-grupo', function(){
	ob_start();

	$lojas = new WP_Query(array('post_type' => 'cpt_lojas'));
	if ($lojas->have_posts()) {
		echo '<div class="container-fluid cf-lojas-pagina-grupo">';
			echo '<div class="row r-lojas-pagina-grupo">';
				while ($lojas->have_posts()) { $lojas->the_post();
					echo '<div class="col-lg-4 bloco-loja">';
						echo '<div class="bloco-loja-interno">';
							echo '<div class="imagem">';
								echo '<img src="'.get_the_post_thumbnail_url().'" class="img-fluid" alt="'.get_the_title().'">';
							echo'</div>';
							echo '<div class="contato">'.get_field('loja_telefones_emails').'</div>';
							echo '<div class="endereco">'.get_field('loja_endereco').'</div>';
						echo'</div>';
					echo'</div>';
				}
			echo'</div>';
		echo'</div>';
	}

	return ob_get_clean();
});

/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
add_shortcode('titulo-pagina-catalogo', function(){
	ob_start();
	?>
	<div class="container-fluid cf-titulo-pagina-catalogo">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h2><?php the_field('catalogo_titulo', 'options'); ?></h2>
				</div>
			</div>
		</div>
	</div>
	<?php
	return ob_get_clean();
});

/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/**
* Custom posts per page for Taxonomy
*/
function cs_custom_posts_per_page( $query ) {
	if ( is_tax( 'tax_produto_cat' ) ) {
		$query->query_vars['posts_per_page'] = -1;
		return;
	}
}
add_filter( 'pre_get_posts', 'cs_custom_posts_per_page' );

/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
add_shortcode('breadcrumb', function(){
	ob_start();
	echo '<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">';
		if(function_exists('bcn_display')) {
			bcn_display();
		}
	echo'</div>';
	return ob_get_clean();
});

/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
function bloco_imovel_catalogo($parametros = array()) {
	$imovel_imagem = (isset($parametros['imagem']) ? $parametros['imagem'] : '');
	$imovel_nome = (isset($parametros['nome']) ? $parametros['nome'] : '');
	$imovel_descricao = (isset($parametros['descricao']) ? $parametros['descricao'] : '');
	$imovel_url = (isset($parametros['url']) ? $parametros['url'] : '');
	
	?>
	<div class="col-lg-12 text-center nopadding bloco-imovel">
		<div class="h-100 w-100 bloco-imovel-interno">
			<a href="<?php echo $imovel_url; ?>">
				<img src="<?php echo $imovel_imagem; ?>" class="img-fluid" alt="<?php echo $imovel_nome; ?>">
				<div class="descricao-bloco">
					<p class="nome"><?php echo $imovel_nome; ?></p>
					<p class="descricao"><?php echo $imovel_descricao; ?></p>
				</div>
			</a>
		</div>
	</div>
	<?php
	
}

/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
function checkCategorias($taxonomy, $parent = 0, $exclude = array()) {
	$categorias = get_terms(
		array(
			'taxonomy' => $taxonomy,
			'hide_empty' => false,
			'parent' => $parent,
			'exclude' => $exclude
		)
	);
	
	// echo '<pre>';
	// echo print_r($categorias);
	// echo '</pre>';

	if (is_array($categorias)) {
		if (count($categorias) >= 1) {
			foreach ($categorias as $categoria) {
				echo '<div class="item-categoria custom-control custom-checkbox">';
					echo '<input type="checkbox" class="custom-control-input" id="check-categoria-'.$categoria->slug.'" name="'.$categoria->slug.'">';
					echo '<label for="check-categoria-'.$categoria->slug.'" class="custom-control-label">'.$categoria->name.'</label>';

					$categorias_filhas = get_term_children($categoria->term_id, $taxonomy);
					if (is_array($categorias_filhas)) {
						if (count($categorias_filhas) >= 1) {
							echo '<div class="categorias-filhas">';
								checkCategorias($taxonomy, $categoria->term_id);
							echo '</div>';
						}
					}


				echo'</div>';
			}
		}
	}
}


/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
/* ===================================================================================================================================== */
add_shortcode('instagram-feed', function(){
	ob_start();

	echo '<div class="container-fluid">';
		echo '<div class="row">';
			echo '<div class="col-lg-12 titulo-feed">';
				echo '<img src="'.FL_CHILD_THEME_URL.'/img/logo-insta-feed.png" alt="" class="logo-1">';
				echo '<img src="'.FL_CHILD_THEME_URL.'/img/logo-nome-insta.png" alt="" class="logo-2">';
			echo '</div>';
			echo '<div class="col-lg-12">';
				echo do_shortcode( '[jr_instagram id="2"]' );
			echo '</div>';
		echo '</div>';
	echo '</div>';

	return ob_get_clean();
});