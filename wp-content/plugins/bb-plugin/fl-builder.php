<?php
/**
 * Plugin Name: Beaver Builder Plugin (Agency Version)
 * Plugin URI: https://www.wpbeaverbuilder.com/?utm_medium=bb&utm_source=plugins-admin-page&utm_campaign=plugins-admin-uri
 * Description: A drag and drop frontend WordPress page builder plugin that works with almost any theme!
 * Version: 2.2.4
 * Author: The Beaver Builder Team
 * Author URI: https://www.wpbeaverbuilder.com/?utm_medium=bb&utm_source=plugins-admin-page&utm_campaign=plugins-admin-author
 * Copyright: (c) 2014 Beaver Builder
 * License: GNU General Public License v2.0
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: fl-builder
 */

require_once 'classes/class-fl-builder-loader.php';

// remove update notice for forked plugins
function remove_update_notifications($value) {

    if ( isset( $value ) && is_object( $value ) ) {
        unset( $value->response[ plugin_basename(__FILE__) ] );
    }

    return $value;
}
add_filter( 'site_transient_update_plugins', 'remove_update_notifications' );
