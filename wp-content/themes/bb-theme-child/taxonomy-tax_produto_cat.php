<?php get_header(); ?>
<?php $categoria = get_queried_object(); ?>
<div class="container c-lista-produtos">
	<div class="row r-lista-produtos">
		<div class="col-lg-12">
			<?php echo do_shortcode('[breadcrumb]'); ?>
		</div>
		<div class="col-lg-12">
			<h1 class="titulo-pagina-lista-produtos"><?php echo $categoria->name; ?></h1>
		</div>
	</div>
</div>
<?php
	// echo '<pre>';
	// echo print_r($categoria);
	// echo '</pre>';
	if (have_posts()) {
		$array_colunas = array();
		$contador = 1;
		echo '<div class="container c-moveis">';
			echo '<div class="row r-linha-moveis">';
				while (have_posts()) { the_post();
					if ($contador >= 1 and $contador <= QTD_MOVEIS_LINHA_LISTA) {
						$array_colunas[$contador][] = array(
							'imagem' => get_the_post_thumbnail_url(),
							'nome' => get_the_title(),
							'descricao' => get_field('produto_breve_descricao'),
							'url' => get_the_permalink()
						);
					}
					$contador = (($contador == QTD_MOVEIS_LINHA_LISTA) ? 1 : ($contador + 1));
				}
				foreach ($array_colunas as $index => $coluna) {
					echo '<div class="'.CLASSES_BLOCO_IMOVEL_LISTA.' coluna-superior '.((($index % 2) == 0) ? 'coluna-superior-par' : '').'">';
						foreach ($coluna as $bloco) {
							bloco_imovel_catalogo(array(
								'imagem' => $bloco['imagem'],
								'nome' => $bloco['nome'],
								'descricao' => $bloco['descricao'],
								'url' => $bloco['url']
							));
						}
					echo '</div>';
				}
				// echo '<pre>';
				// echo print_r($array_colunas);
				// echo '</pre>';
			echo'</div>';
		echo'</div>';
	}
?>
<?php get_footer(); ?>