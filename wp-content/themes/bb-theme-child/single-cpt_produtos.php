<?php get_header(); ?>
<?php the_post(); ?>
<div class="container c-movel-interna">
	<div class="row r-movel-interna">
		<div class="col-lg-12 bloco-breadcrumb">
			<?php echo do_shortcode('[breadcrumb]'); ?>
		</div>
		<div class="col-lg-6 bloco-galeria">
			<div class="bloco-galeria-interno">
				<?php $fotos = get_field('produto_galeria'); ?>
				<?php if ($fotos) { ?>
					<?php foreach ($fotos as $foto) { ?>
						<div class="imagem-galeria">
							<a data-fancybox="gallery" href="<?php echo $foto; ?>">
								<img src="<?php echo $foto; ?>" alt="<?php the_title(); ?>">
							</a>
						</div>
					<?php } ?>
				<?php } else { ?>
					<div class="imagem-galeria">
						<a data-fancybox="gallery" href="<?php echo get_the_post_thumbnail_url(); ?>">
							<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
						</a>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="col-lg-6 bloco-dados-movel">
			<div class="titulo-ref">
				<h2 class="titulo-movel"><?php the_title(); ?></h2>
				<?php if (get_field('produto_referencia')) { ?>
					<p>ref.: <?php the_field('produto_referencia'); ?></p>
				<?php } ?>
			</div>
			<div class="bloco-descricao">
				<p class="titulo-descricao">DESCRIÇÃO:</p>
				<div class="descricao"><?php the_content(); ?></div>
			</div>
		</div>
	</div>
</div>
<div class="container c-formulario">
	<div class="row r-formulario">
		<div class="col-lg-6">
			<div class="compartilhe">
				<h3 class="titulo-secao">COMPARTILHE</h3>
				<a style="text-decoration: none;" href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
					<img class="btn_share" src="<?php echo home_url('wp-content/uploads/2019/08/icone-prod-facebook.png'); ?>" title="Compartilhar no Facebook">
				</a>
				<a style="text-decoration: none;" href="https://twitter.com/intent/tweet/?url=<?php the_permalink(); ?>&amp;text=<?php the_title(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
					<img class="btn_share" src="<?php echo home_url('wp-content/uploads/2019/08/icone-prod-twitter.png'); ?>" title="Compartilhar no Twitter">
				</a>
				<a href="https://wa.me/?text=<?php the_permalink(); ?>" target="_blank">
					<img class="btn_share" src="<?php echo home_url('wp-content/uploads/2019/08/icone-prod-whatsapp.png'); ?>" title="Compartilhar no WhatsApp">
				</a>
			</div>
			<div class="contatos">
				<?php
					$lojas = new WP_Query(
						array(
							'post_type' => 'cpt_lojas',
							'posts_per_page' => -1
						)
					);
				?>
				<?php if ($lojas->have_posts()) { ?>
					<h3 class="titulo-secao">CONTATOS</h3>
					<?php while ($lojas->have_posts()) { $lojas->the_post(); ?>
						<div class="bloco-loja">
							<p class="titulo"><?php the_title(); ?></p>
							<div class="contato"><?php the_field('loja_contatos'); ?></div>
						</div>
					<?php } wp_reset_postdata(); ?>
				<?php } ?>
			</div>
		</div>
		<div class="col-lg-6 coluna-formulario-cf7">
			<h3 class="titulo-secao">FALE CONOSCO</h3>
			<?php echo do_shortcode('[contact-form-7 id="229" title="Contato Produto"]'); ?>
		</div>
	</div>
</div>
<div class="container c-relacionados">
	<div class="row r-relacionados">
		<div class="col-lg-12 col-relacionados">
			<h2 class="titulo-movel">TALVEZ VOCÊ TAMBÉM GOSTE DE:</h2>
			<div class="produtos-carroussel">
				<?php
					$relacionados = get_the_terms(get_the_ID(), 'tax_produto_cat');
					$categorias = array();
					foreach ($relacionados as $relacionado) {
						$categorias[] = $relacionado->slug;
					}
					// echo '<pre>';
					// echo print_r($categorias);
					// echo '</pre>';

					$produtos_rel = new WP_Query(
						array(
							'post_type' => 'cpt_produtos',
							'posts_per_page' => -1,
							'tax_query' => array(
								array(
									'taxonomy' => 'tax_produto_cat',
									'field' => 'slug',
									'terms' => $categorias
								)
							)
						)
					);

					if ($produtos_rel-> have_posts()) {
						while ($produtos_rel-> have_posts()) { $produtos_rel-> the_post();
							?>
							<div class="produto">
								<div class="produto-interno" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');">
									<a href="<?php the_permalink(); ?>">
										<img src="<?php echo FL_CHILD_THEME_URL.'/img/product-rel.svg'; ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>">
									</a>
								</div>
							</div>
							<?php
						} wp_reset_postdata();
					}
				?>
			</div>
		</div>		
	</div>
</div>
<script>
	jQuery(document).ready(function($) {
		$('.bloco-galeria-interno').slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1
		});
		$('.produtos-carroussel').slick({
			infinite: true,
			slidesToShow: 5,
			slidesToScroll: 1
		});
	});
</script>
<?php get_footer(); ?>