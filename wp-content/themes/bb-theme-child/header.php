<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php do_action( 'fl_head_open' ); ?>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php echo apply_filters( 'fl_theme_viewport', "<meta name='viewport' content='width=device-width, initial-scale=1.0' />\n" ); ?>
<?php echo apply_filters( 'fl_theme_xua_compatible', "<meta http-equiv='X-UA-Compatible' content='IE=edge' />\n" ); ?>
<link rel="profile" href="https://gmpg.org/xfn/11" />
<?php

wp_head();

FLTheme::head();

?>
<script src="<?php echo FL_CHILD_THEME_URL.'/js/jquery-ui.js'; ?>"></script>
<link rel="stylesheet" href="<?php echo FL_CHILD_THEME_URL.'/css/jquery-ui.css'; ?>">
</head>
<body <?php body_class(); ?><?php FLTheme::print_schema( ' itemscope="itemscope" itemtype="https://schema.org/WebPage"' ); ?>>
<?php

FLTheme::header_code();

do_action( 'fl_body_open' );

?>
<div class="fl-page">
	<?php

	do_action( 'fl_page_open' );

	FLTheme::fixed_header();

	do_action( 'fl_before_top_bar' );

	FLTheme::top_bar();

	do_action( 'fl_after_top_bar' );
	do_action( 'fl_before_header' );

	// FLTheme::header_layout();
	?>
<header class="fl-page-header fl-page-header-primary fl-page-nav-right fl-page-nav-toggle-button fl-page-nav-toggle-visible-medium-mobile fl-shrink-header-transition" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
	<div class="fl-page-header-wrap">
		<div class="fl-page-header-container container">
			<div class="fl-page-header-row row">
				<div class="col-md-12 col-lg-4 fl-page-header-logo-col">
					<div class="fl-page-header-logo" itemscope="itemscope" itemtype="https://schema.org/Organization">
						<a href="<?php echo home_url(); ?>" itemprop="url"><?php FLTheme::logo(); ?></a>
					</div>
				</div>
				<div class="col-md-12 col-lg-8 fl-page-nav-col">
					<div class="fl-page-nav-wrap">
						<nav class="fl-page-nav fl-nav navbar navbar-default navbar-expand-md" aria-label="Menu do Cabeçalho" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
							<button type="button" class="navbar-toggle navbar-toggler" data-toggle="collapse" data-target=".fl-page-nav-collapse">
								<span>Menu</span>
							</button>
							<div class="fl-page-nav-collapse collapse navbar-collapse">
								<?php
									//FLTheme::nav_search();
									wp_nav_menu(
										array(
											'theme_location' => 'header',
											'items_wrap' => '<ul id="%1$s" class="nav navbar-nav %2$s">%3$s</ul>',
											'container' => false,
											'fallback_cb' => 'FLTheme::nav_menu_fallback'
										)
									);
								?>
							</div>
							<div class="redes-sociais">
								<div class="d-inline-block rede-social rede-social-facebook">
									<a href="#">
										<img src="<?php echo FL_CHILD_THEME_URL.'/svg/facebook.svg'; ?>" alt="Facebook">
									</a>
								</div>
								<div class="d-inline-block rede-social rede-social-instagram">
									<a href="#">
										<img src="<?php echo FL_CHILD_THEME_URL.'/svg/instagram.svg'; ?>" alt="Facebook">
									</a>
								</div>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<script>
	jQuery(document).ready(function($) {
		$('.item-menu-contato a').on('click', function(event) {
			event.preventDefault();
			$('html').animate({
				scrollTop: $(".titulo-fale-conosco").offset().top - 120
			}, 500);
		});
	});
</script>
	<?php

	do_action( 'fl_after_header' );
	do_action( 'fl_before_content' );

	?>
	<div class="fl-page-content" itemprop="mainContentOfPage">

		<?php do_action( 'fl_content_open' ); ?>
