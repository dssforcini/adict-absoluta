<?php get_header(); ?>
<section class="pagina-interna pagina-interna-catalogo">
	<?php echo do_shortcode('[titulo-pagina-catalogo]'); ?>
	<div class="container-fluid cf-busca-catalogo">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 bloco-refinamento">
					<div class="bloco-refinamento-interno">
						
						<form action="" method="POST" class="formBuscaCatalogo">

							<div class="bloco bloco-busca">
								<p class="titulo-refinamento">refinamento.</p>
								<div class="campo-busca">
									<input type="text" name="txtBuscaCatalogo" class="txtBuscaCatalogo" placeholder="o que você procura?">
									<button class="btn btn-default" type="button">
										<img src="<?php echo FL_CHILD_THEME_URL.'/svg/icone-buscar.svg'; ?>" class="img-fluid" alt="Buscar" title="Buscar">
									</button>
								</div>
							</div>
							<div class="bloco bloco-categoria">
								<div class="botao">
									<button class="btn btn-primary collapsed" type="button" data-toggle="collapse" data-target="#collapseCategoria" aria-expanded="false" aria-controls="collapseCategoria">Categoria<span class="sinal"></span></button>
								</div>
								<div class="conteudo collapse" id="collapseCategoria">
									<?php
										/*
										$categorias = get_terms(
											array(
												'taxonomy' => 'tax_produto_cat',
												'hide_empty' => false,
												'parent' => 0
											)
										);
										foreach ($categorias as $categoria) {
											echo '<div class="item-categoria custom-control custom-checkbox">';
												echo '<input type="checkbox" class="custom-control-input" id="check-cat-'.$categoria->slug.'" name="'.$categoria->slug.'">';
												echo '<label for="check-cat-'.$categoria->slug.'" class="custom-control-label">'.$categoria->name.'</label>';
											echo'</div>';
										}
										*/
										checkCategorias('tax_produto_cat', 0, array(35));
									?>
								</div>
							</div>
							<?php /* ?>
							<div class="bloco bloco-preco">
								<div class="botao">
									<button class="btn btn-primary collapsed" type="button" data-toggle="collapse" data-target="#collapsePreco" aria-expanded="false" aria-controls="collapsePreco">Preço<span class="sinal"></span></button>
								</div>
								<div class="conteudo collapse" id="collapsePreco">
									<div class="item-preco">
										<input type="hidden" name="valor-minimo" class="valor-minimo">
										<input type="hidden" name="valor-maximo" class="valor-maximo">
										<label for="amount">Arraste para definir os valores:</label>
										<input type="text" id="amount" readonly style="border:0; color:#010101; font-weight:bold; text-align: center;border-radius: 0px;">
										<div id="slider-range"></div>
										<?php
											$valor_minimo = 0;
											$valor_maximo = 0;
											$moveis = new WP_Query(array('post_type' => 'cpt_produtos', 'posts_per_page' => -1));
											if ($moveis->have_posts()) {
												while ($moveis->have_posts()) { $moveis->the_post();
													$valor_movel = get_field('produto_valor');

													// pega o menor valor
													if ($valor_minimo == 0) {
														$valor_minimo = $valor_movel;
													} else {
														if ($valor_movel <= $valor_minimo) {
															$valor_minimo = $valor_movel;
														}
													}

													// pega o maior valor
													if ($valor_maximo == 0) {
														$valor_maximo = $valor_movel;
													} else {
														if ($valor_movel >= $valor_maximo) {
															$valor_maximo = $valor_movel;
														}
													}
												}
												// echo 'VALOR MINIMO: '.$valor_minimo.'<br>';
												// echo 'VALOR MAXIMO: '.$valor_maximo.'<br>';
											}
										?>
										<script>
											jQuery(document).ready(function($) {
												$( "#slider-range" ).slider({
													range: true,
													step: 10,
													animate: "fast",
													min: <?php echo $valor_minimo; ?>,
													max: <?php echo $valor_maximo; ?>,
													values: [ <?php echo ($valor_minimo + 1000); ?>, <?php echo ($valor_maximo - 1000); ?> ],
													slide: function( event, ui ) {
														$( "#amount" ).val( "R$ " + ui.values[ 0 ] + " - R$ " + ui.values[ 1 ] );
														$('.valor-minimo').val(ui.values[ 0 ]);
														$('.valor-maximo').val(ui.values[ 1 ]);
													}
												});
												$( "#amount" ).val( "R$ " + $( "#slider-range" ).slider( "values", 0 ) + " - R$ " + $( "#slider-range" ).slider( "values", 1 ) );
											});
										</script>
									</div>
								</div>
							</div>

							<div class="bloco bloco-cor">
								<div class="botao">
									<button class="btn btn-primary collapsed" type="button" data-toggle="collapse" data-target="#collapseCor" aria-expanded="false" aria-controls="collapseCor">Cor<span class="sinal"></span></button>
								</div>
								<div class="conteudo collapse" id="collapseCor">
									<?php
										$categorias = get_terms(
											array(
												'taxonomy' => 'tax_produto_cor',
												'hide_empty' => false,
												'parent' => 0
											)
										);
										foreach ($categorias as $categoria) {
											echo '<div class="item-categoria custom-control custom-checkbox">';
												echo '<input type="checkbox" class="custom-control-input" id="check-cor-'.$categoria->slug.'" name="'.$categoria->slug.'">';
												echo '<label for="check-cor-'.$categoria->slug.'" class="custom-control-label">'.$categoria->name.'</label>';
											echo'</div>';
										}
									?>
								</div>
							</div>
							<div class="bloco bloco-marca">
								<div class="botao">
									<button class="btn btn-primary collapsed" type="button" data-toggle="collapse" data-target="#collapseMarca" aria-expanded="false" aria-controls="collapseMarca">Marca<span class="sinal"></span></button>
								</div>
								<div class="conteudo collapse" id="collapseMarca">
									<?php
										$categorias = get_terms(
											array(
												'taxonomy' => 'tax_produto_marca',
												'hide_empty' => false,
												'parent' => 0
											)
										);
										foreach ($categorias as $categoria) {
											echo '<div class="item-categoria custom-control custom-checkbox">';
												echo '<input type="checkbox" class="custom-control-input" id="check-marca-'.$categoria->slug.'" name="'.$categoria->slug.'">';
												echo '<label for="check-marca-'.$categoria->slug.'" class="custom-control-label">'.$categoria->name.'</label>';
											echo'</div>';
										}
									?>
								</div>
							</div>
							<?php */ ?>
							<div class="bloco bloco-ambiente">
								<div class="botao">
									<button class="btn btn-primary collapsed" type="button" data-toggle="collapse" data-target="#collapseAmbiente" aria-expanded="false" aria-controls="collapseAmbiente">Ambiente<span class="sinal"></span></button>
								</div>
								<div class="conteudo collapse" id="collapseAmbiente">
									<?php
										/*
										$categorias = get_terms(
											array(
												'taxonomy' => 'tax_produto_cat',
												'hide_empty' => false,
												'parent' => 35
											)
										);
										foreach ($categorias as $categoria) {
											echo '<div class="item-categoria custom-control custom-checkbox">';
												echo '<input type="checkbox" class="custom-control-input check-ambientes" id="check-ambiente-'.$categoria->slug.'" name="'.$categoria->slug.'">';
												echo '<label for="check-ambiente-'.$categoria->slug.'" class="custom-control-label">'.$categoria->name.'</label>';
												echo '<div class="item-filho">';

												echo '</div>';
											echo'</div>';
										}
										*/
										checkCategorias('tax_produto_cat', 35);
									?>
								</div>
							</div>
							

						</form>

					</div>
				</div>
				<div class="col-lg-8 bloco-imoveis">
					<?php if (count($_GET) == 0) { ?>
						<?php
							$produtos = new WP_Query(
								array(
									'post_type' => 'cpt_produtos'
								)
							);
						?>
					<?php } else { ?>
						
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>